let mix = require('laravel-mix');

mix.js('resources/js/main.js', 'public/js');
mix.sass('resources/scss/app.scss', 'public/css');

