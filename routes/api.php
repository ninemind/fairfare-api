<?php

use Laravel\Lumen\Routing\Router;

/** @var Router $router */
$router->group(['prefix' => 'api'], function() use ($router) {

    $router->group(['prefix' => 'wdw'], function() use ($router) {

        $router->group(['prefix' => 'parks'], function() use ($router) {
            $router->get('/', ['uses' => 'ParkController@all']);
            $router->get('/{id}', ['uses' => 'ParkController@one']);
            $router->get('/{id}/attractions', ['uses' => 'ParkController@attractions']);
        });

        $router->group(['prefix' => 'attractions'], function() use ($router) {
            $router->get('/', ['uses' => 'AttractionController@all']);
            $router->get('/{id}', ['uses' => 'AttractionController@one']);
        });

        $router->group(['prefix' => 'guests'], function() use ($router) {
            $router->get('/', ['uses' => 'GuestController@all']);
            $router->post('/', ['uses' => 'GuestController@create']);
            $router->get('/{id}', ['uses' => 'GuestController@one']);
            $router->delete('/{id}', ['uses' => 'GuestController@delete']);
        });

        $router->group(['prefix' => 'park-visits'], function() use ($router) {
            $router->get('/', ['uses' => 'ParkVisitController@all']);
            $router->post('/', ['uses' => 'ParkVisitController@create']);
            $router->get('/{id}', ['uses' => 'ParkVisitController@one']);
            $router->delete('/{id}', ['uses' => 'ParkVisitController@delete']);

            $router->put('/{id}/guest', ['uses' => 'ParkVisitController@addGuest']);
        });

        $router->group(['prefix' => 'fastpass-requests'], function() use ($router) {
            $router->get('/', ['uses' => 'FastpassRequestController@all']);
            $router->post('/', ['uses' => 'FastpassRequestController@create']);
            $router->get('/{id}', ['uses' => 'FastpassRequestController@one']);
            $router->delete('/{id}', ['uses' => 'FastpassRequestController@delete']);
        });

    });

});
