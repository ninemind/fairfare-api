<?php

namespace App\Api\Controllers;

use App\ParkVisit;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ParkVisitController extends Controller
{
    public function all()
    {
        return response()->json(
            ParkVisit
                ::with('park', 'guests')
                ->whereDate('date', '>', Carbon::yesterday('America/New_York'))
                ->orderBy('date', 'ASC')
                ->orderBy('entry', 'ASC')
                ->get()
        );
    }

    public function one(int $id)
    {
        return response()->json(
            ParkVisit
                ::with('park', 'guests')
                ->findOrFail($id)
        );
    }

    public function create(Request $request)
    {
        $parkVisit = ParkVisit::create($request->all());

        return response()->json($parkVisit, 201);
    }

    public function addGuest(Request $request, int $id)
    {
        $parkVisit = ParkVisit::find($id);
        $parkVisit->guests()->attach($request->get('guest_id'));

        return response()->json(
            $parkVisit
                ->load('guests')
                ->load('park'),
            200
        );
    }

    public function delete(int $id)
    {
        ParkVisit::findOrFail($id)->delete();

        return response()->json(['id' => $id], 200);
    }
}
