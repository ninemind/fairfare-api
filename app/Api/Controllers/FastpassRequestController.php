<?php

namespace App\Api\Controllers;

use App\FastpassRequest;
use Illuminate\Http\Request;

class FastpassRequestController extends Controller
{
    public function all()
    {
        return response()->json(
            FastpassRequest
                ::with('parkVisit', 'attraction', 'guests')
                ->orderBy('created_at', 'ASC')
                ->get()
        );
    }

    public function one(int $id)
    {
        return response()->json(
            FastpassRequest
                ::with('parkVisit', 'attraction', 'guests')
                ->findOrFail($id)
        );
    }

    public function create(Request $request)
    {
        $fastpassRequest = FastpassRequest::create($request->except('guests'));

        if ($request->has('guests')) {
            $fastpassRequest->guests()->attach($request->input('guests'));
        }

        // hydrate
        $fastpassRequest->parkVisit;
        $fastpassRequest->attraction;
        $fastpassRequest->guests;

        return response()->json($fastpassRequest, 201);
    }

    public function delete(int $id)
    {
        FastpassRequest::findOrFail($id)->delete();

        return response()->json(['id' => $id], 200);
    }
}
