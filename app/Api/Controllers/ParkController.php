<?php

namespace App\Api\Controllers;

use App\Attraction;
use App\Park;

class ParkController extends Controller
{
    public function all()
    {
        return response()->json(
            Park::all()
        );
    }

    public function attractions(int $id)
    {
        return response()->json(
            Attraction
                ::where('park_id', $id)
                ->orderBy('name', 'ASC')
                ->get()
        );
    }

    public function one(int $id)
    {
        return response()->json(
            Park
                ::with('attractions')
                ->findOrFail($id)
        );
    }
}
