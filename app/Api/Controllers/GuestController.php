<?php

namespace App\Api\Controllers;

use App\Guest;
use Illuminate\Http\Request;

class GuestController extends Controller
{
    public function all()
    {
        return response()->json(Guest::all());
    }

    public function one(string $id)
    {
        return response()->json(
            Guest
                ::with('parkVisits.park')
                ->findOrFail($id)
        );
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'id'   => 'required|unique:wdw_guests|regex:/^[A-Z0-9]{8}-\d{4}-\d{4}-\d{4}-\d{12}$/',
            'name' => 'required|unique:wdw_guests|max:32'
        ]);

        $guest = Guest::create($request->all());

        return response()->json($guest, 201);
    }

    public function delete(string $id)
    {
        Guest::findOrFail($id)->delete();

        return response()->json(['id' => $id], 200);
    }
}
