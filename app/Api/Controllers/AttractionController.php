<?php

namespace App\Api\Controllers;

use App\Attraction;

class AttractionController extends Controller
{
    public function all()
    {
        return response()->json(
            Attraction
                ::with('park')
                ->orderBy('name', 'ASC')
                ->get()
        );
    }

    public function one(int $id)
    {
        return response()->json(
            Attraction
                ::with('park')
                ->findOrFail($id)
        );
    }
}
