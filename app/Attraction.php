<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attraction extends Model
{
    protected $table = 'wdw_attractions';

    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'id',
        'name',
        'park_id'
    ];

    protected $hidden = [
        'park_id'
    ];


    public function park()
    {
        return $this->belongsTo('App\Park');
    }
}
