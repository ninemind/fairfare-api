<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DisableMigrateFreshCommand extends Command
{
    protected $signature = 'migrate:fresh';

    protected $description = 'Disable migrate fresh command in Artisan';

    public function handle()
    {
        $this->warn('migrate:fresh has been disabled.  Don\'t EVER user it on this project!');
    }
}
