<?php

namespace App\Console\Commands;

use App\FastpassRequest;
use App\Services\DisneyWorldApiClient;
use Illuminate\Console\Command;

class SecureFastpassesCommand extends Command
{
    protected $signature = 'wdw:fastpasses:secure';

    protected $description = 'Secure fastpasses';

    public function handle()
    {
        /** @var DisneyWorldApiClient $wdw */
        $wdw = app()->make(DisneyWorldApiClient::class);

        $fastPassRequests = FastpassRequest
            ::with('parkVisit', 'attraction', 'guests')
            ->orderBy('created_at', 'ASC')
            ->get();

        foreach ($fastPassRequests as $fastPassRequest) {
            var_dump($wdw->getFastPassOffers($fastPassRequest)); die;
        }
    }
}
