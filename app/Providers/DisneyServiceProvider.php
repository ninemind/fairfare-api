<?php

namespace App\Providers;

use App\Services\DisneyWorldApiClient;
use Illuminate\Support\ServiceProvider;

class DisneyServiceProvider extends ServiceProvider
{
    /**
     * @return void
     */
    public function register()
    {
        $this->app->singleton(DisneyWorldApiClient::class, function($app) {
            return new DisneyWorldApiClient();
        });
    }
}
