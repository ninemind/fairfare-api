<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Park extends Model
{
    protected $table = 'wdw_parks';

    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'id',
        'name'
    ];

    protected $hidden = [];


    public function attractions()
    {
        $this->hasMany('App\Attraction');
    }
}
