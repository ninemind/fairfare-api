<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParkVisit extends Model
{
    protected $table = 'wdw_park_visits';

    public $incrementing = true;
    public $timestamps = false;

    protected $fillable = [
        'park_id',
        'date',
        'entry',
        'exit'
    ];

    protected $hidden = [
        'park_id',
        'pivot'
    ];


    public function park()
    {
        return $this->belongsTo('App\Park');
    }

    public function guests()
    {
        return $this->belongsToMany('App\Guest', 'wdw_guest_visit');
    }
}
