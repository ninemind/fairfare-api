<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guest extends Model
{
    protected $table = 'wdw_guests';

    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'id',
        'name'
    ];

    protected $hidden = [
        'pivot'
    ];


    public function parkVisits()
    {
        return $this->belongsToMany('App\ParkVisit', 'wdw_guest_visit');
    }

    public function fastPassRequests()
    {
        return $this->belongsToMany('App\FastpassRequest', 'wdw_fastpass_guest');
    }
}
