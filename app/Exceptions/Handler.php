<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    public function render($request, Exception $e)
    {
        $rendered = parent::render($request, $e);

        if ($request->isJson()) {
            $error = $e->getMessage();
            $fields = [];

            if (empty($errors)) {
                if ($e instanceof MethodNotAllowedHttpException) {
                    $error = 'Method not allowed.';
                }
            }

            if ($e instanceof ValidationException) {
                $fields = json_decode($e->getResponse()->getContent());
            } else if ($e instanceof QueryException) {
//                $error = 'An error has occurred.';
            } else if ($e instanceof ModelNotFoundException) {
                $error = 'Entity not found.';
            }

            return response()->json([
                'error' => [
                    'message' => $error,
                    'fields'  => $fields
                ]
            ], $rendered->getStatusCode());
        }

        return $rendered;
    }
}
