<?php namespace App\Services;

use App\FastpassRequest;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class DisneyWorldApiClient
{
    const BASE_URL = 'https://disneyworld.disney.go.com';
    const AUTH_ENDPOINT = 'authentication/get-client-token';


    /** @var Client **/
    private $client;

    /** @var string */
    private $token;

    /** @var int */
    private $tokenExpiration = 0;


    public function __construct()
    {
        $this->client = new Client;
    }

    /**
     * @param string $verb
     * @param string $endpoint
     * @param array $params
     * @param bool $auth
     * @return mixed
     * @throws GuzzleException
     */
    public function call(string $verb, string $endpoint, array $params = [], $auth = true)
    {
        $responseTime = null;
        $url = join('/', [self::BASE_URL, $endpoint]);

        $options['headers']['Content-Type'] = 'application/json;charset=UTF-8';

        if ($auth) {
            if (strtotime('-15 seconds') >= $this->tokenExpiration) {
                $this->updateToken();
            }

            $options['headers']['Authorization'] = 'BEARER ' . $this->token;
        }

        if (!empty($params)) {
            $options[$verb === 'GET' ? 'query' : 'json'] = $params;
        }

        try {
            $res = $this->client->request($verb, $url, $options);
            return json_decode($res->getBody());
        } catch (ClientException $e) {
            // TODO: log error
            var_dump(json_decode($e->getResponse()->getBody()->getContents()));
            die;
        }
    }

    /**
     * @throws GuzzleException
     */
    protected function updateToken()
    {
        $response = $this->call('GET', self::AUTH_ENDPOINT, [], false);

        $this->token = $response->access_token;
        $this->tokenExpiration = strtotime("+$response->expires_in seconds");
    }

    /**
     * @return array
     * @throws GuzzleException
     */
    public function getAttractions()
    {
        $attractions = $this->call(
            'GET',
            'api/wdpro/explorer-service/public/finder/map/ancestor/80007798;entityType=destination;destination=80007798?filters=Attraction&&region=us'
        );

        return (array) $attractions->results;
    }

    /**
     * @param int $attractionId
     * @return object
     * @throws GuzzleException
     */
    public function getAttractionDetails(int $attractionId)
    {
        $details = $this->call(
            'GET',
            'finder/api/v1/explorer-service/details-entity-simple/wdw/' . $attractionId . ';entityType=Attraction/' . date('Y-m-d')
        );

        return $details;
    }

    /**
     * @return object
     * @throws GuzzleException
     */
    public function getFastPassOffers(FastpassRequest $fastPassRequest)
    {
        $guestIds = $fastPassRequest->guests->map(function($guest) {
            return $guest->id;
        })->toArray();

        $offers = $this->call(
            'POST',
            sprintf('wdpro-wam-fastpassplus/api/v1/fastpass/orchestration/park/%d/%s/offers;start-time=%s;end-time=%s;guest-xids=%s/?experienceId=%d',
                $fastPassRequest->parkVisit->park->id,
                $fastPassRequest->parkVisit->date,
                $fastPassRequest->parkVisit->entry,
                $fastPassRequest->parkVisit->exit,
                implode(',', $guestIds),
                $fastPassRequest->attraction->id
            )
        );

        return $offers;
    }
}
