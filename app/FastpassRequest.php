<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FastpassRequest extends Model
{
    protected $table = 'wdw_fastpass_requests';

    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'park_visit_id',
        'attraction_id'
    ];
    protected $hidden = [
        'park_visit_id',
        'attraction_id',
        'updated_at'
    ];


    public function guests()
    {
        return $this->belongsToMany('App\Guest', 'wdw_fastpass_guest');
    }

    public function parkVisit()
    {
        return $this->belongsTo('App\ParkVisit');
    }

    public function attraction()
    {
        return $this->belongsTo('App\Attraction');
    }
}
