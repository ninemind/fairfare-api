import Vue from 'vue';
import App from './App'
import { store } from './store/store'
import moment from 'moment'

new Vue({
    store,
    components: { App },
    template: '<App/>'
}).$mount('#app');

Vue.filter('date', (value) => {
    if (!value) return '';

    return moment(value, 'YYYY-MM-DD').format('ddd, MMM D, YYYY');
});

Vue.filter('time', (value) => {
    if (!value) return '';

    return moment(value, 'HH:mm:ss').format('h:mma');
});
