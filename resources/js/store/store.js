import Vue from 'vue';
import 'es6-promise/auto'
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        loading: true,
        error: null,
        visit: null,
        guests: [],
        attraction: null,
        fastPassRequest: null,
        time: null
    },
    mutations: {
        setVisit(state, visit) {
            state.visit = visit
        },
        loading(state) {
            state.loading = true
        },
        finishedLoading(state) {
            state.loading = false
        },
        setError(state, error) {
            state.error = error
        },
        setGuests(state, guests) {
            state.guests = guests
        },
        setAttraction(state, attraction) {
            state.attraction = attraction
        },
        setFastPassRequest(state, fastPassRequest) {
            state.fastPassRequest = fastPassRequest
        }
    },
    getters: {
        loading: state => state.loading,
        error: state => state.error,
        visit: state => state.visit,
        guests: state => state.guests,
        partySize: (state, getters) => getters.guests.length,
        attraction: state => state.attraction,
        fastPassRequest: state => state.fastPassRequest,
        time: state => state.time
    }
});
