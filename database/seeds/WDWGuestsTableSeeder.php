<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class WDWGuestsTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('wdw_guests')->insert([
            'id' => 'CAFEF00D-6502-0000-0000-000302263931',
            'name' => 'Aurora'
        ]);

        DB::table('wdw_guests')->insert([
            'id' => 'CAFEF00D-6502-0000-0000-000056501115',
            'name' => 'Christopher'
        ]);

        DB::table('wdw_guests')->insert([
            'id' => 'CAFEF00D-6502-0000-0000-000229311431',
            'name' => 'Ciera'
        ]);

        DB::table('wdw_guests')->insert([
            'id' => 'CAFEF00D-6502-0000-0000-000224690943',
            'name' => 'Julie'
        ]);

        DB::table('wdw_guests')->insert([
            'id' => 'CAFEF00D-6502-0000-0000-000016749135',
            'name' => 'Travis'
        ]);
    }
}
