<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class WDWParksTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('wdw_parks')->insert([
            'id' => 80007823,
            'name' => 'Animal Kingdom'
        ]);

        DB::table('wdw_parks')->insert([
            'id' => 80007838,
            'name' => 'Epcot'
        ]);

        DB::table('wdw_parks')->insert([
            'id' => 80007944,
            'name' => 'Magic Kingdom'
        ]);

        DB::table('wdw_parks')->insert([
            'id' => 80007998,
            'name' => 'Hollywood Studios'
        ]);
    }
}
