<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
         $this->call([
             WDWGuestsTableSeeder::class,
             WDWParksTableSeeder::class,
             WDWAttractionsTableSeeder::class
         ]);
    }
}
