<?php

use App\Attraction;
use App\Services\DisneyWorldApiClient;
use Illuminate\Database\Seeder;

class WDWAttractionsTableSeeder extends Seeder
{
    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function run()
    {
        /** @var DisneyWorldApiClient $wdw */
        $wdw = app()->make(DisneyWorldApiClient::class);
        $attractions = $wdw->getAttractions();

        foreach($attractions as $attraction) {
            if (empty($attraction->fastPassPlus)) {
                continue;
            }

            $attraction->name = str_replace('™', '', $attraction->name);
            $attraction->name = str_replace(['-', '–'], '-', $attraction->name);

            Attraction::firstOrCreate([
                'id'   => (int) $attraction->id,
                'name' => explode(' - ', $attraction->name)[0],
                'park_id' => (int) $attraction->parkIds[0]
            ]);
        }
    }
}
