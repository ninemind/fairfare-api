<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWdwGuestVisitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wdw_guest_visit', function (Blueprint $table) {
            $table->primary(['park_visit_id', 'guest_id']);
            $table->unique(['park_visit_id', 'guest_id']);
            $table
                ->integer('park_visit_id')
                ->unsigned();
            $table
                ->foreign('park_visit_id')
                ->references('id')
                ->on('wdw_park_visits')
                ->onDelete('cascade')
            ;
            $table->string('guest_id', 36);
            $table
                ->foreign('guest_id')
                ->references('id')
                ->on('wdw_guests')
                ->onDelete('cascade')
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wdw_guest_visit');
    }
}
