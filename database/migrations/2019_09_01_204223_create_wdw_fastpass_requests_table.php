<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWdwFastpassRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wdw_fastpass_requests', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table
                ->integer('park_visit_id')
                ->unsigned();
            $table
                ->foreign('park_visit_id')
                ->references('id')
                ->on('wdw_park_visits')
                ->onDelete('cascade')
            ;
            $table
                ->integer('attraction_id')
                ->unsigned();
            $table
                ->foreign('attraction_id')
                ->references('id')
                ->on('wdw_attractions')
                ->onDelete('cascade')
            ;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wdw_fastpass_requests');
    }
}
