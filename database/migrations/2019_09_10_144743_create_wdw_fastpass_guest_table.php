<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWdwFastpassGuestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wdw_fastpass_guest', function (Blueprint $table) {
            $table->primary(['fastpass_request_id', 'guest_id']);
            $table->unique(['fastpass_request_id', 'guest_id']);
            $table
                ->integer('fastpass_request_id')
                ->unsigned();
            $table
                ->foreign('fastpass_request_id')
                ->references('id')
                ->on('wdw_fastpass_requests')
                ->onDelete('cascade')
            ;
            $table->string('guest_id', 36);
            $table
                ->foreign('guest_id')
                ->references('id')
                ->on('wdw_guests')
                ->onDelete('cascade')
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wdw_fastpass_guest');
    }
}
