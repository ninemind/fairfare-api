<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWdwParkVisitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wdw_park_visits', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table
                ->integer('park_id')
                ->unsigned();
            $table
                ->foreign('park_id')
                ->references('id')
                ->on('wdw_parks')
                ->onDelete('cascade')
            ;
            $table->date('date');
            $table->time('entry');
            $table->time('exit');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wdw_park_visits');
    }
}
